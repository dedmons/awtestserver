
/*
 * GET home page.
 */

var multiparty = require('multiparty')
  , util = require('util')


exports.index = function (req, res) {
  res.render('index', { title: 'Index' });
}

exports.redir = function(req, res) {
  console.log("Redirecting");
  res.redirect('/chunk');
};

exports.mag = function(req, res){
  console.log("Header: "+JSON.stringify(req.headers,null,4));
  if (typeof req.headers.via !== "undefined" && req.headers.via !== null)
    res.render('index', { title: 'Express' });
  else
    res.send(404)
};

exports.nomag = function(req, res){
  console.log("Header: "+JSON.stringify(req.headers,null,4));
  if (typeof req.headers.via !== "undefined" && req.headers.via !== null)
    res.send(404)
  else
    res.render('index', { title: 'Express' });
};


exports.getPoster = function(req, res){
  res.header('Accept-Encoding', 'identity');
  res.render('postform', { title: 'Post Something' });
};

exports.postPoster = function(req, res){
  var form = new multiparty.Form();

  form.parse(req, function(err, fields, files) {
    res.writeHead(200, {'content-type': 'text/plain'});
    res.write('received upload:\n\n');
    res.end(util.inspect({fields: fields, files: files},{ depth: null }));
  });
};

exports.chunk = function(req, res){
  res.setHeader('Content-Type', 'text/html; charset=UTF-8');
  res.setHeader('Transfer-Encoding', 'chunked');

  var html =
    '<!DOCTYPE html>' +
    '<html lang="en">' +
    '<head>' +
    '<meta charset="utf-8">' +
    '<title>Chunked transfer encoding test</title>' +
    '</head>' +
    '<body>';

  res.write(html);

  html = '<h1>Chunked transfer encoding test</h1>'

  res.write(html);

  // Now imitate a long request which lasts 5 seconds.
  setTimeout(function(){
    html = '<h5>This is a chunked response after 5 seconds. The server should not close the stream before all chunks are sent to a client.</h5>'

    res.write(html);

    // since this is the last chunk, close the stream.
    html =
      '</body>' +
      '</html';

    res.end(html);

  }, 10000);

  // this is another chunk of data sent to a client after 2 seconds before the
  // 5-second chunk is sent.
  setTimeout(function(){
    html = '<h5>This is a chunked response after 2 seconds. Should be displayed before 5-second chunk arrives.</h5>'

    res.write(html);

  }, 5000);
};
