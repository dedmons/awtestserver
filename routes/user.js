
/*
 * GET users listing.
 */
var userDB = require('../models/user.js');

exports.add = function(req, res){
  var user = [];
  user.name = req.body.name;
  user.pass = req.body.pass;
  userDB.upsert(user, function(err){
    if (err) {
      console.log(err);
      res.send(500);
    } else {
      res.send(201, 'User Updated');
    }
  });
};

exports.form = function(req, res){
  res.render('userform');
};

exports.auth = function(req, res){
  console.log(req.user);
  if (req.user !== undefined)
    res.send(200, "You Made It!!!!");
  else{
    res.set('WWW-Authenticate', 'Basic');
    res.send(401, "Not Allowed");
  }
};
