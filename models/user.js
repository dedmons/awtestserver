var fs = require('fs');
var path = require('path');
var userFile = path.join(__dirname,'../data/users');

exports.upsert = function(user, callback){
  fs.readFile(userFile, 'utf8', function(err, data){
    if(err){
      callback(err);
    }
    var users = new Object();
    if (data !== undefined && data !== "" && data !== null)
      users = JSON.parse(data);
    console.log(users);
    if (user.pass !== '') {
      users[user.name] = user.pass;
    } else {
      delete users[user.name];
    }
    console.log(user);
    console.log(users);
    console.log(JSON.stringify(users));
    fs.writeFile(userFile, JSON.stringify(users), function(err){
      callback(err);
    });
  });
};

exports.check = function(user, callback){
  fs.readFile(userFile, 'utf8', function(err, data){
    if (err) callback(err, false);
    users = JSON.parse(data);
    if (users[user.name] === user.pass) {
      callback(null, true);
    } else {
      callback(null, false);
    }
  });
};
