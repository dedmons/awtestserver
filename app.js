
/**
 * Module dependencies.
 */

var express = require('express');
var routes = require('./routes');
var user = require('./routes/user');
var userDB = require('./models/user.js');
var http = require('http');
var path = require('path');
//var busboy = require('connect-busboy');

var app = express();
app.use(function(req, res, next) {
  req.headers['if-none-match'] = 'no-match-for-this';
  res.set('Cache-Control', 'no-cache');
  next();
});
// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(express.favicon());
//app.use(busboy({ immediate: true }));
app.use(function(req, res, next){
  console.log('================================');
  console.log('Request info');
  console.log('ip: '+req.ip);
  console.log('time: '+Date());
  console.log(req.headers);
  console.log('================================');
  next();
});
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}


var auth = express.basicAuth(function(user, pass, fn) {
  console.log('Authenticating U: '+user+' P: '+pass);
  var userOBJ = new Array();
  userOBJ.name = user;
  userOBJ.pass = pass;
  userDB.check(userOBJ, function(err, authed){
    if (err) {
      fn(err, null);
    } else {
      fn(null, authed);
    }
  });
});


app.get('/',       routes.index);
app.get('/mag',    routes.mag);
app.get('/nomag',  routes.nomag);
app.get('/chunk',  routes.chunk);
app.get('/redir',  routes.redir);
app.get('/auth',   auth, user.auth);
app.get('/auth2',  auth, user.auth);
app.get('/user',   user.form);
app.post('/user',  user.add);
app.get('/poster', routes.getPoster);
app.post('/poster', routes.postPoster);

http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});
